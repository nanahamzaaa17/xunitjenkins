using System;
using Xunit;
using SquareLib;


namespace SquareUnitTest
{
    public class SquareUnit
    {
        [Fact]
        public void TestCalc()
        {
            //Arrange
            double number = 3;
            double expected = 9;
            Square square = new Square();

            //Act
            double actual = square.calc(number);
            //Assert

            Assert.Equal(expected, actual);
        }
    }
}
