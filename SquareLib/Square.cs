﻿using System;

namespace SquareLib
{
    public class Square
    {

        public double calc(double number)
        {
            return number * number;
        }
               
    }
}
